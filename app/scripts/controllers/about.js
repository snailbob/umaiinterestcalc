'use strict';

/**
 * @ngdoc function
 * @name umaiInterestCalcApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the umaiInterestCalcApp
 */
angular.module('umaiInterestCalcApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
