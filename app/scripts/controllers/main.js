'use strict';

/**
 * @ngdoc function
 * @name umaiInterestCalcApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the umaiInterestCalcApp
 */
angular.module('umaiInterestCalcApp')
  .controller('MainCtrl', ['$scope', function ($scope) {
		$scope.compoundMode = false;
		$scope.freq = '1';
		$scope.interest = 0;
    $scope.amount = 0;
    $scope.calcTable = [];
		
		$scope.toggleMode = function(bool) {
			$scope.compoundMode = bool;
			$scope.calcInterest();
		};
		
		$scope.initValues = function() {
			$scope.principal = null;
			$scope.rate = null;
			$scope.time = null;
			$scope.freq = '1';
			$scope.amount = 0;
      $scope.interest = 0;
      $scope.calcTable = [];
    };
    
		$scope.calcInterest = function() {
      var p = $scope.principal;
      var n = $scope.time;
      var r = $scope.rate;
      var forTable = {};
      var incYear = 1;

      $scope.calcTable = [];
      $scope.interest = 0;
      $scope.amount = 0;

			if(!$scope.compoundMode) {
				if(p !== null && n !== null && r !== null) {
					var _interest = 0;
					var interest = (p*n*r)/100;
					$scope.interest = interest;
          $scope.amount = p + interest;

          for(incYear; incYear <= n; incYear++){
            _interest = (p*incYear*r)/100;

            forTable = {
              interest: _interest,
              amount: p + _interest
            };
            $scope.calcTable.push(forTable);
          }

				}
			}
			else {
        n = $scope.freq;
        var t = $scope.time;
        
				if(p !== null && r !== null && n !== null && t !== null) {
          var _amount = 0;
					var amount = (p * Math.pow(1+(r/(100*n)), n*t));
					$scope.interest = amount - p;
          $scope.amount = amount;
          incYear = 1;

          for(incYear; incYear <= t; incYear++){
            _amount = (p * Math.pow(1+(r/(100*n)), n*incYear));
            forTable = {
              amount: _amount,
              interest: _amount - p
            };
            $scope.calcTable.push(forTable);
          }

				}
			}
    };
    $scope.initValues();

  }]);
